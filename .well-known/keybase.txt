==================================================================
https://keybase.io/jonohill
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://jonohill.nz
  * I am jonohill (https://keybase.io/jonohill) on keybase.
  * I have a public key ASDuUR_9UotzJxm038MaY5qW37Ra9XIbSiW-p0ScI6R1pQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120e58e4fc1eb234922c2b937732b7922b637f761ccb5278d66c23925fba54e8b080a",
      "host": "keybase.io",
      "kid": "0120ee511ffd528b732719b4dfc31a639a96dfb45af5721b4a25bea7449c23a475a50a",
      "uid": "2cc2feff02c989644f176eed288ba519",
      "username": "jonohill"
    },
    "merkle_root": {
      "ctime": 1553839345,
      "hash": "03bb1f688df7e4f7cb3d8c7d6119380d437e3025bb750050b39e50a01d5573676038a9d79d5dbc5ca52088f7e85cae7f9d42bea3d0606efe3bcf73559ad94530",
      "hash_meta": "0736bf02baac657b36f76c2954b095ee5eae5d248b3c8332e8eb0311dd20c889",
      "seqno": 5052390
    },
    "service": {
      "entropy": "j4WAP404d4WEZY7NBF18SEFw",
      "hostname": "jonohill.nz",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.1.2"
  },
  "ctime": 1553839376,
  "expire_in": 504576000,
  "prev": "2601c31d232e7cf15a6a1aadcadb3f490a7b45f3ff6b88a11aee0eb7993fcdfd",
  "seqno": 18,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg7lEf/VKLcycZtN/DGmOalt+0WvVyG0olvqdEnCOkdaUKp3BheWxvYWTESpcCEsQgJgHDHSMufPFaahqtyts/SQp7RfP/a4ihGu4Ot5k/zf3EIMgLHsddWZgfT6BgRNG49OkjBLOG3AFEqKjMc5/4X/DxAgHCo3NpZ8RA6r7QpIdcnpfF/+n407CJHclis0LOOFXx3xTtdlVtoHjmO09muzo4uzvBQ1ujw+f7Xk1yuhUxjbPfIEt9yoFXAahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIIEuFTaCvv1GFQRjOLAkc0EuwsrtbpYbz95BGGq3uvhxo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/jonohill

==================================================================