#!/usr/bin/env bash

set -e

url_length=$(twurl /1.1/help/configuration.json | jq '.short_url_length_https')
max_tweet_length=$(( 270 - $url_length ))
>&2 echo "Tweet length: $max_tweet_length"

comm -13 <(sort _site_prev/posts.txt) <(sort _site/posts.txt) | \
while IFS=$"\n" read -r post; do
    url=$(echo "$post" | cut -d' ' -f1)
    title=$(echo "$post" | cut -d' ' -f2- | cut -c 1-$max_tweet_length)
    [[ "${#title}" == "$max_tweet_length" ]] && title="$title"...
    status="$title $url"
    >&2 echo "New post: $status"

    twurl -d "status=$status" /1.1/statuses/update.json
done
