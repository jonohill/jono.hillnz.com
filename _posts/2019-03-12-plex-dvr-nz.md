---
layout: post
title: "Plex DVR in NZ, with no TV tuner or aerial"
date: 2019-03-11 21:03:24 +1300
categories: plex docker
published: true
---

![Plex TV Guide](/images/plex-tvguide.png)

[Plex DVR](https://www.plex.tv/tv/) is a very nice way to view or record live TV content. Like everything Plex, its strength is in the number of ways you can watch - they have clients for any device you can imagine and you can easily share with people inside or outside your home. There are also other nice features, like automatic ad removal.

The only problem is you need to have both a physical TV tuner and an aerial, both of which can be impractical depending on your setup. If you're in New Zealand, here's a way to use Plex DVR without needing any physical hardware.

You will need a working Plex Server with a [Plex Pass](https://www.plex.tv/plex-pass/). If you don't have a Plex Pass they're often discounted around US holidays - keep an eye on their [Twitter](https://twitter.com/plex).

## Overview

1. [Matt Huisman](https://www.matthuisman.nz) provides excellent IPTV feeds for Freeview.
2. These are interpreted by [tellytv](https://github.com/tellytv/telly), an IPTV proxy that pretends to be a hardware tuner.
3. Plex sees what it thinks is a tuner so you can watch and record as you wish.

On the surface it's actually quite simple! That's a good thing, and so far I've also found it quite reliable. The caveat is that not every channel is streamed online so you'll find those are missing - Prime is the most obvious example.

## tellytv

tellytv emulates a physical TV tuner so that Plex thinks it's talking to real hardware. There are a few different options out there (try Googling Plex IPTV) but I've found this one the most reliable and versatile. An important feature for us is that tellytv supports passing the streams via FFmpeg - Plex is quite fussy about the stream it gets so FFmpeg can sit in the middle to convert the Freeview HTTP live streams into plain mpegts streams without CPU intensive transcoding.

I use Docker to run Plex. If you don't, you'll need to find a way to install FFMpeg and tellytv onto your Plex system - [Download FFMpeg](https://www.ffmpeg.org/download.html) and try following the [guides on tellytv's Github wiki](https://github.com/tellytv/telly/wiki).

I decided to have Plex and tellytv/FFmpeg in the same Docker container. In my opinion this is preferable since it eases networking and means less to manage, and the two are very intertwined anyway. You can pull this image at [`jonoh/plex-tellytv`](https://hub.docker.com/r/jonoh/plex-tellytv)

If you prefer to build it yourself:

`run-tellytv.sh`
```shell
#!/usr/bin/with-contenv bash

cd /config
exec s6-setuidgid plex /opt/tellytv
```
Plex uses [s6 overlay](https://github.com/just-containers/s6-overlay) for init so this script will be executed at container run to start tellytv. Remember to make it executable (`chmod +x run-tellytv.sh`).

`Dockerfile`
```Dockerfile
FROM plexinc/pms-docker:latest

RUN apt-get update && apt-get install -y \
        ffmpeg \
    && rm -rf /var/lib/apt/lists/*

COPY --from=tellytv/telly:dev /app /opt/tellytv
ADD run-tellytv.sh /etc/services.d/tellytv/run
```

tellytv is itself published as a Docker image so it's easy to copy it to the official Plex image. Note also the inclusion of FFmpeg.

### tellytv config

The tellytv config is used to set the source for the IPTV channels. Don't worry too much about terminology here - for our purposes IPTV means an `m3u8` file containing a list of channels and their stream URLs.

You've probably noticed that you can watch the likes of TV1 etc on their websites, and the good news is thanks to [Matt Huisman](https://www.matthuisman.nz) we have a curated list of these streams published as an `m3u8` file. The magic URL is `http://i.mjh.nz/nz/raw-tv.m3u8`. Here's what my tellytv config file looks like:

```toml
[Discovery]                                    
  Device-Auth = "telly123"                     
  Device-ID = 12345678                         
  Device-UUID = ""
  Device-Firmware-Name = "hdhomeruntc_atsc"
  Device-Firmware-Version = "20150826"
  Device-Friendly-Name = "telly"
  Device-Manufacturer = "Silicondust"
  Device-Model-Number = "HDTC-2US"
  SSDP = true

[IPTV]
  Streams = 3
  Starting-Channel = 1
  XMLTV-Channels = true
  FFMpeg = true             

[Log]
  Level = "warn"
  Requests = false

[Web]
  Base-Address = "127.0.0.1:6077"
  Listen-Address = "0.0.0.0:6077"

[[Source]]
  Name = "FreeviewNZ"
  Provider = "Custom"
  M3U = "http://i.mjh.nz/nz/raw-tv.m3u8"
  Filter = ""
  FilterKey = ""
  FilterRaw = false
  Sort = ""
```

There's nothing here you need to change but you should review [the docs](https://github.com/tellytv/telly/wiki).
If you're using the Docker image, save this as `telly.config.toml` in your Plex config directory (whatever mounts to `/config`). Remember to restart your container.

## Plex

Open your server settings -> Live TV & DVR -> Set up Plex DVR.

![Plex DVR - Device](/images/plex-dvrdevice.png)

Plex should find "telly". If not, choose "Don't see your HDHomeRun device..." and enter the "Base-Address" from your tellytv config.

⚠️ Plex seems to have a bug that means even after entering the address nothing may appear. As long as the Continue button is enabled you are good to go, just click it.

Plex will (via tellytv) pick up the available channels. Change the country to New Zealand to continue.

![Plex DVR - Channel Scan](/images/plex-channelscan.png)

Before going further, a word on guide data. Plex has built in data, but unfortunately it seems to be [a bit shit for New Zealand](https://forums.plex.tv/t/poor-progam-guide-information-in-new-zealand/227249).

Instead, take advantage of Plex's XMLTV support to point to a better data source. Some choices:
- For data from Freeview: `https://nzxmltv.github.io/xmltv/guide.xml`.
- For data from Sky I have created [a scraper](https://gitlab.com/jonohill/xmltv-skynz).

There are other sources of this data but I don't recommend them. Both of the above have logic to detect movies versus TV shows which is important for Plex to save recordings to the right place.

Having figured out your source, choose the XMLTV option and enter either a local or HTTP path.

![Plex DVR - XMLTV](/images/plex-xmltv.png)

Review the channel mappings - Plex takes a guess but it pays to review. You can also remove any stupid channels.

Plex will take some time to populate the guide... a good time to review the DVR settings and turn on Remove commercials.

We're done! Go and check the guide and try watching something.
